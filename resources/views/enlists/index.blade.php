@extends('enlists.layout')

@section('content')
<div class=pull-left>
    <h2>CRUD APP</h2>
</div>

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-right">
            <a class="btn btn-success" href="{{route('enlists.create')}}">Create New Profile</a>
        </div>
    </div>
</div>

@if($message=Session::get('success'))
    <div class="alert alert-success">
        <p>{{$message}}</p>
    </div>
@endif

<table class="table table-bordered">
    <tr>
        <th>No</th>
        <th>Name</th>
        <th>Course</th>
        <th>Salary</th>
        <th width="280px">Action</th>
    </tr>
    @foreach($enlists as $enlist)

    <tr>
        <td>{{++$i}}</td>
        <td>{{$enlist->name}}</td>
        <td>{{$enlist->job}}</td>
        <td>{{$enlist->salary}}</td>
        <td>
            <form action="{{route('enlists.destroy',$enlist->id)}}"method="POST"
                <a class="btn btn-info" href="{{route('enlists.show',$enlist->id)}}">Show</a>
                
                <a class="btn btn-primary" href="{{route('enlists.edit',$enlist->id)}}">Edit</a>

                @csrf

                @method('DELETE')

                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>