@extends('enlists.layout')

@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2> Edit Product</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{route('enlists.index')}}">Back</a>
        </div>
    </div>
</div>

@if($error->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong>There were some problems with your input.<br><br>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endofreach
        </ul>
    </div>
@endif

<form action="{{route('enlists.update','enlists->id)}}"method="POST">
    @csrf
    @method('PUT')

    @foreach($enlists as $enlist)

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <strong>Name:</strong>
            <input type="text" name="name" value="{{$enlist->name}}" class="form-control" placeholder="Name">
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Job:</strong>
                <input type="text" name="job" value="{{$enlist->job}}" class="form-control" placeholder="Job">
            </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Salary</strong>
                    <input type="text" name="salary" value="{{$enlist->salary}}" class="form-control" placeholder="salary">
            </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
    </div>
</form>

@endsection

