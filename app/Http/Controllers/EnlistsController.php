<?php

namespace App\Http\Controllers;

use App\Models\Enlists;
use Illuminate\Http\Request;

class EnlistsController extends Controller
{
   public function index()
   {
       $enlists = Enlists::latest()->paginate(5);
       return view('enlists.index',compact('enlists'))
        ->with('i', (request()->input('page', 1) - 1) * 5);
   }

   public function create()
   {
       return view('enlists.create');
   }

   public function store(Request $request)
   {
       $request->validate([
           'name'=>'required',
           'job'=>'required',
           'salary'=>'required',

       ]);

       Enlists::create($request->all());

       return redirect()->route('enlists.index')
        ->with('success','Enlisted successfully.');
   }

   public function show(Enlists$enlists)
   {
       return view('enlist.show',compact('enlist'));

   }

   public function edit(Enlists$enlists)
   {
       return view('enlist.edit',compact('enlist'));
   }

   public function update(Request$request, Enlists$enlists)
   {
       $request->validate([

       ]);

       $student->update($request->all());

       return redirect()->route('enlist.index')
        ->with('success','Enlisted successfully.');
   }

   public function destroy(Enlists$enlists)
   {
       $enlists->delete();

       return redirect()->route('enlists.index')
        ->with('success','Profile deleted successfully');
   }

}
